<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <!-- Page formatting-->
    <link rel="stylesheet" type="text/css" href="css/product-add_design.css">
    <meta name="keywords" content="Test, Scandiweb, create product">
    <meta name="description" content="Junior developer test create a new product ">
    <!-- Fit page in all devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add product</title>
    <script src="js/product-add.js"></script>
</head>
<body>
<?php
    include "include/main.php";
?>
    <div class="container">
        <div class="row">
            <div class="col-12 col--title" >
                <div class="product-add-head-title">
                    <h1 class="heading-main"> Product Add </h1>
                </div>
                <div class="product-add-save">
                    <button id="save-btn" class="btn btn--apply" type="submit" form="add-frm"> Save </button>
                </div>
						</div>
        </div>
        <hr class="line">
        <div class="row frm--row">
            <form action="include/main.php" method="post" id="add-frm" class="frm-new-product">
                <div class="frm--new-product__row">
                    <label class="input--label" for="sku-field">SKU: </label>
                    <input class="input--field" name="sku-field" id="sku-field" type="text" required>
                </div>
                <div class="frm--new-product__row">
                    <label class="input--label" for="name-field">Name: </label>
                    <input class="input--field" name="name-field" id="name-field" type="text" required>
                </div>
                <div class="frm--new-product__row">
                    <label class="input--label" for="price-field">Price: </label>
                    <input class="input--field" name="price-field" id="price-field" type="number" required>
                </div>
                <div class="frm--new-product__row">
                    <label class="input--label" for="type-field">Type: </label>
                    <select id="type-field" name = "type-field" onChange="chosen_option()" >
                        <option selected="selected" disabled >Select product type </option>
                        <option value="DVD_disc"> DVD </option>
                        <option value="Book"> Book </option>
                        <option value="Furniture"> Furniture </option>
                    </select>
                </div>
                <span id="additional-form-field"></span>
            </form>
        </div>
    </div>
</body>
</html>