<!DOCTYPE html>
<head>
    <title>Product list</title>
    <meta charset="UTF-8">
    <!-- Page formatting-->
    <link rel="stylesheet" type="text/css" href="css/catalog_design.css">
    <meta name="keywords" content="Test, Scandiweb, catalog">
    <meta name="description" content="Junior developer test catalog ">
    <!-- Fit page in all devices-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<?php
    include "include/main.php";
?>
<div class="container">
    <div class="row">
        <div class="col-12 col--title" >
            <div class="catalog-head-title">
                <h1 class="heading-main"> Product list </h1>
            </div>
            <div class="catalog-mass-delete-action">
                <span> Mass Delete Action: </span>
                <button id="delete-btn" class="btn--apply" type="submit"> Apply </button>
            </div>
        </div>
    </div>
    <hr class="line">
    <div class="row">
        <div class="col-12">
            <!--<div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="34">
                <span class="product-sku"> SKU</span>
                <span class="product-name"> name</span>
                <span class="product-price">Price $$$</span>
                <span class="product-extra-field"> Kind of extra field!</span>
            </div>
            <div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="34">
                <span class="product-sku"> SKU</span>
                <span class="product-name"> name</span>
                <span class="product-price">Price $$$</span>
                <span class="product-extra-field"> Kind of extra field!</span>
            </div>
            <div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="34">
                <span class="product-sku"> SKU</span>
                <span class="product-name"> name</span>
                <span class="product-price">Price $$$</span>
                <span class="product-extra-field"> Kind of extra field!</span>
            </div>
            <div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="34">
                <span class="product-sku"> SKU</span>
                <span class="product-name"> name</span>
                <span class="product-price">Price $$$</span>
                <span class="product-extra-field"> Kind of extra field!</span>
            </div>
            <div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="34">
                <span class="product-sku"> SKU</span>
                <span class="product-name"> name</span>
                <span class="product-price">Price $$$</span>
                <span class="product-extra-field"> Kind of extra field!</span>
            </div> -->
					<?php
					$productArr = Product::getAllProducts();
					foreach($productArr as $item){
					//var_dump($item);?>
					<div class="card">
                <input name="productId[]" class="delete-checkbox" type="checkbox" value="<?php echo $item['id']; ?>">
                <span class="product-sku"> <?php echo $item['sku']; ?></span>
                <span class="product-name"> <?php echo $item['name']; ?></span>
                <span class="product-price"><?php echo $item['price']." $"; ?></span>
                <span class="product-extra-field"> <?php
										if(isset($item['size']))
											echo "Size: ".$item['size']." MB";
										else if (isset($item['weight']))
											echo "Weight: ".$item['weight']." KG";
										else if (isset($item['dimensions']))
											echo "Dimensions: ".$item['dimensions'];
					?> </span>
            </div>
					<?php }?>
        </div>
			<div>
				<p>Hello!</p>
			</div>
    </div>
</div>
</body>


