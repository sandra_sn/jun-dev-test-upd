//Function for dynamic form changes
function chosen_option(){
    var option = document.getElementById("type-field").value;
    var xhttp = new XMLHttpRequest();
    //alert(option);
    switch(option){
        case "DVD_disc":{
            document.getElementById("additional-form-field").innerHTML = "";
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("additional-form-field").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "include/dvd-form.php",true);
            xhttp.send();
            break;
        }
        case "Book":{
            document.getElementById("additional-form-field").innerHTML = "";
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("additional-form-field").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "include/book-form.php",true);
            xhttp.send();

            break;
        }
        case "Furniture":{
            document.getElementById("additional-form-field").innerHTML = "";
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("additional-form-field").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", "include/furniture-form.php",true);
            xhttp.send();

            break;
        }
        default:
            alert("File does not exist!!");
    }
}

//Function for furniture dimesions correct formating
function set_dimensions(){
    var dimensions;
    var length = document.getElementById("length").value;
    var width = document.getElementById("width").value;
    var height = document.getElementById("height").value;

    dimensions = height +"x"+width+"x"+length;
    document.getElementById("add-property").value = dimensions;
    //alert(dimensions);
}
