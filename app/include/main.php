<?php

	//Database connection class
	class Database{
		
		private $service = "database";
		private $username = "root" ;
		private $password  = "root";
		private $database = "catalog" ;
		private $port = "3306";
		private static $instance;
		
		//Singleton
		public static function getInstance(){
			if(!self::$instance){
				self::$instance = new self();
			}
			return self::$instance;
		}
		
		private function __construct(){
			$this->conn = new mysqli($this->service, $this->username, $this->password, $this->database, $this->port);
			
			if(mysqli_connect_error()){
				echo "Connection error: ".mysqli_connect_error();
			}
		}
		
		protected function connect(){
			return $this->conn;
		}	
	}
	
	//Other main classes 
	class Product extends Database {

		public $sku;
		public $name;
		public $price;
		
		public function __construct($sku,$name,$price){
			$this->sku = $sku;
			$this->name = $name;
			$this->price = $price;
		}
		
		public static function getAllProducts(){
			$db = Database::getInstance();
			$ask = "SELECT * FROM products";
			$result = $db->connect()->query($ask);
			$resultRows = $result->num_rows;
			if($resultRows > 0){
				while($product = $result->fetch_assoc() ){
					$products[] = $product;
				}
				return $products;
			}
			else
					echo "No products in database...";
				return 0;
		}
		
		protected function insertInDatabase($insertInfo){
			$db = Database::getInstance();
			$insert = $db->connect()->query($insertInfo);
			//echo $insertInfo;
			if(!$insert){
				echo "Error.";
				echo "$insertInfo"; E_USER_ERROR;
			}
			else{
				//echo "Everything fine!!! Hurray!";
				header('Location:../productAdd.php');
			}
		}
		
		public static function deleteFromDatabase($ids){
			$db = Database::getInstance();
			foreach($ids as $id){
				$deleteQuery = "DELETE FROM `products` WHERE id='$id'";
				$delete = $db->connect()->query($deleteQuery);
			}
			
			if(!$delete){
				//echo "Error.";
				echo "$deleteQuery"; E_USER_ERROR;
			}
			else{
				//echo "Everything fine!!! Hurray!";
				header('Location:../catalog.php');
			}
		}
		
		public function getProduct(){
			echo $this->sku . "<br>";
			echo $this->name . "<br>";
			echo $this->price . "<br>";
		}
		
		public function __destruct(){}
	}
	
	class DVD_disc extends Product{
		public $size;
		
		public function setDVD_disc($size){
			$this->size = $size;
			$this->makeQuery();
		}
		public function makeQuery(){
			$queryStr = "INSERT INTO `products`(`sku`, `name`, `price`, `size`) VALUES ('$this->sku', '$this->name','$this->price', '$this->size')";
			//echo $queryStr;
			
			$insert = $this->insertInDatabase($queryStr);
		}
	}
	
	class Book extends Product{
		public $weight;
		
		public function setBook($weight){
			$this->weight = $weight;
			$this->makeQuery();
		}
		
		public function makeQuery(){
			$queryStr = "INSERT INTO `products`(`sku`, `name`, `price`, `weight`) VALUES ('$this->sku', '$this->name', '$this->price', '$this->weight')";
			
			$insert = $this->insertInDatabase($queryStr);
		}
	}
	
	class Furniture extends Product{
		public $dimensions;
		
		public function setFurniture($dimensions){
			//$this->dimensions = "{$height}x{$width}x{$lenght}";
			$this->dimensions = $dimensions;
			$this->makeQuery();
		}
		
		public function makeQuery(){
			$queryStr = "INSERT INTO `products`(`sku`, `name`, `price`, `dimensions`) VALUES ('$this->sku', '$this->name', '$this->price', '$this->dimensions')";
			
			$insert = $this->insertInDatabase($queryStr);
		}
		
		public function getDimensions(){
			echo $this->dimensions;
		}
	}

// Recived data manipulations

//For saving data in DB.	
if(isset($_POST['type-field'])){
	$productType = htmlspecialchars($_POST['type-field']);
	//echo $productType;
	$product = new $productType(htmlspecialchars($_POST['sku-field']),htmlspecialchars($_POST['name-field']),htmlspecialchars($_POST['price-field']));
	$function = "set{$productType}";
	$product->$function(htmlspecialchars($_POST['add-property']));
	$product=null;
}

//For removing data from DB.	
if(isset($_POST['productId'])){
	//var_dump($_POST['productId']);
	Product::deleteFromDatabase($_POST['productId']);
}


	
	
	
?>