<!DOCTYPE html>
<html>
    <div class="frm--new-product__row">
        <label class="input--label" for="height">Height:</label>
        <input type="number" id="height" name="height" onblur="set_dimensions()" required>
    </div>
    <div class="frm--new-product__row">
        <label class="input--label" class="input--label" for="width">Width:</label>
        <input type="number" id="width" name="width" onblur="set_dimensions()" required>
    </div>
    <div class="frm--new-product__row">
        <label class="input--label" for="length">Length:</label>
        <input class="input--field" type="number" id="length" name="length" onblur="set_dimensions()" required>
    </div>
        <div class="frm--new-product__row">
        <span class="label"> Please, provide dimensions in HxWxL format! </span>
    </div>
        <input value="" id="add-property" name="add-property" type="hidden">
    </div>
</html>